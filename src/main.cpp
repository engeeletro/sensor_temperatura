#include "Saiot.h"
#include <DHT.h> 

#define DHTPIN D2
#define DHTTYPE DHT11 

float temperatura; 
float temperaturaval = 0.0;
float umidade; 
float umidadeval = 0.0; 

DHT dht(DHTPIN, DHTTYPE); 

String deviceName = "Sensor Temperatura";
String deviceClass = "Temperatura/Umidade";
String deviceDescription = "Dispositivo que lê dados de Temp e Umidade";

Saiot saiot(deviceName, deviceClass, deviceDescription);

String tempCallback();
String umiCallback();

void setup()
{
  Serial.begin(9600);

  Sensor *DHT1 = new Sensor("temp", "Temperatura", "number", {"0"}, 5, 20, tempCallback);
  Sensor *DHT2 = new Sensor("umid", "Umidade", "number", {"0"}, 5, 20, umiCallback);
  saiot.addSensor(*DHT1);
  saiot.addSensor(*DHT2);

  saiot.start();
}

void loop()
{
 saiot.loop();
}


String tempCallback()
{
  temperatura = dht.readTemperature();

  if(!isnan(temperatura)  && temperatura<50 && temperatura>0)
  {
    temperaturaval = temperatura;
    return String(temperatura);
  }
  
  return String(temperaturaval);
}

String umiCallback()
{
  umidade = dht.readHumidity();

  if(!isnan(umidade) && umidade < 95 && umidade > 15)
  {
    umidadeval = umidade;
    return String(umidade);
  }
  return String(umidadeval);
}
