#include "Saiot.h"

#define Red D7
#define Green D6
#define Blue D5

String deviceName = "Lampada RGB";
String deviceClass = "lampada";
String deviceDescription = "Lampada RGB funcionando";

Saiot saiot(deviceName, deviceClass, deviceDescription);

String RGBCallback(String value[]);


void setup()
{
  Serial.begin(9600);

  pinMode(Red, OUTPUT);
  pinMode(Green, OUTPUT);
  pinMode(Blue, OUTPUT);
  
  Actuator *led = new Actuator("RGB", "Multicolor", "color", {"0","0","0","0"}, RGBCallback);
  saiot.addActuator(*led);
  
  saiot.disableOTA();

  saiot.start();
}

void loop()
{
  saiot.loop();
}

String RGBCallback(String value[])
{

  String rgbx[] = {value[0],value[1],value[2],value[3]};

  float gvalue = (value[3].toFloat() / 255);

  for(int i(0);i<4;i++)
  {
     rgbx[i] = String(gvalue * rgbx[i].toFloat());
  }

  analogWrite(Red, rgbx[0].toInt());
  analogWrite(Green, rgbx[1].toInt());
  analogWrite(Blue, rgbx[2].toInt());

  return *value;
}
