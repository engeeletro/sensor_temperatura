#include <Arduino.h>

#include "Saiot.h"

#define TEMP_PIN A0

String deviceName = "Temperatura";
String deviceClass = "temp";
String deviceDescription = "Temperatura com LM35";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do sensor
String tempCallback();

void setup()
{
  Serial.begin(9600);

  Sensor *temp = new Sensor("temp", "temp", "number", {"0"}, 5, 20, tempCallback);
  saiot.addSensor(*temp);
  saiot.start();
}

void loop()
{
  saiot.loop();
}

String tempCallback()
{
  int valorObtido = analogRead(TEMP_PIN);

  float milivolts = (valorObtido / 1024.0) * 3300;
  float celsius = milivolts / 10;
  return String(celsius);
}
