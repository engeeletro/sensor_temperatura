#ifndef SAIOT_H
#define SAIOT_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <LittleFS.h>
#include <time.h>
#include <ArduinoOTA.h>

#include "Actuator.h"
#include "Sensor.h"

// #define EEPROM_SIZE 3

class Saiot
{
private:
    WiFiClient espClient;
    PubSubClient pubSubClient;
    WiFiManager wifiManager;

    const char *broker_url = "dev.saiot2.ect.ufrn.br"; //"192.168.0.105"
    const int broker_port = 8000;             //3002
    long tz = -3;
    byte dst = 0;
    bool hasValidId = false;
    bool hasValidId2 = false;
    bool firsttime = false;
    String id;
    String email;
    String password;
    String deviceName;
    String deviceClass;
    String deviceDescription;
    unsigned int qtdActuators = 0;
    Actuator *actuators[5];
    unsigned int qtdSensors = 0;
    Sensor *sensors[5];
    boolean logged;
    char emailr[50];
    char senhar[20];

    WiFiManagerParameter custom_senha{"SENHA", "senha", senhar, 20};
    WiFiManagerParameter custom_email{"EMAIL","email",emailr,50};



    boolean enableOTAStart = true;
    bool otastart = false;

    void connectWifi();
    bool saveaccount();
    bool checkaccount();
    void connectMqtt();
    void reconnect();

    String json();
    void subscribe(const char *topic);
    void sendMessage(const char *topic, const char *payload);
    void mqttCallback(char *topic, byte *payload, unsigned int length);

    String formatData(String id, String value);
    String formatData(String id, String value, String time);

    void sendData(String sensorId, String data);
    void sendData(String sensorId, String data, String time);

    String formattedTime(time_t temp);
    /*
    bool existQueue(int i);
    void startQueue(int i);
    void finishQueue(int i);
    bool recQueue(int i);
    void refreshQueue(int i, bool sent);
    void smartQueue(bool senddata);*/
    
public:
    Saiot(String deviceName,
          String deviceClass,
          String deviceDescription);
    ~Saiot();

    void addActuator(Actuator &newActuator);
    void addSensor(Sensor &newSensor);

    void start();
    boolean loop();

    void actState(const char *topic, const char *payload, int actnumb, String newState);
    void enableOTA();
    void handleOTA();
    void disableOTA();
};

//Função callback do servidor NTP em time.h, delay do refresh

#endif