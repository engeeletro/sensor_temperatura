#ifndef Actuator_H
#define Actuator_H

#include <Arduino.h>

#define CALLBACK_ACTUATOR_TYPE std::function<String(String value[])>

class Actuator
{
private:
    String id;
    String name;
    String type;
    std::vector<String> value;
    CALLBACK_ACTUATOR_TYPE callback;

public:
    Actuator(String id, String name, String type, std::vector<String> value, CALLBACK_ACTUATOR_TYPE callback);
    ~Actuator();
    String json();
    String getId();
    String callCallback(String value[]);
};

#endif