#include "Saiot.h"

/*!
 * @brief  class constructor
 * @param outPin analog pin connected to OUT
 */
Saiot::Saiot(String deviceName,
             String deviceClass,
             String deviceDescription)
{
  this->deviceName = deviceName;
  this->deviceClass = deviceClass;
  this->deviceDescription = deviceDescription;
  this->logged = false;

  // Procurando pelo id do dispositivo
  //LittleFS.format();
  if (LittleFS.begin())
  {
    if(!checkaccount())
    {
      id = "ESP8266Client-";
      id += String(random(0xffff), HEX);
    }
  }
  else
  {
    // Serial.println("LittleFSWWTEY3HRUStkTFR mount failed...");

    id = "ESP8266Client-";
    id += String(random(0xffff), HEX);
  }

  pubSubClient.setClient(espClient);
  pubSubClient.setBufferSize(512);
  pubSubClient.setServer(broker_url, broker_port);
  pubSubClient.setCallback([this](char *topic, byte *payload, unsigned int length) { this->mqttCallback(topic, payload, length); });
}

/*!
 * @brief  class constructor with possibility to turn sensor ON/OFF
 * @param outPin analog pin connected to OUT
 * @param enablePin digital pin connected to EN, 
 *   this is used for turning the sensor ON/OFF
 */
Saiot::~Saiot()
{
}

void Saiot::addActuator(Actuator &newActuator)
{
  actuators[qtdActuators++] = &newActuator;
}

void Saiot::addSensor(Sensor &newSensor)
{
  sensors[qtdSensors++] = &newSensor;
}

void Saiot::start()
{
  connectWifi();
  //enableOTA();
  configTime(tz * 3600, dst, "pool.ntp.org", "time.nist.gov");
  /*
  for(unsigned int i(0); i<qtdSensors; i++)
  {
    if(existQueue(i))
    {
      recQueue(i);
    }
  }
  */
  connectMqtt();
}

boolean Saiot::loop()
{
  if(WiFi.status() == WL_CONNECTED && !hasValidId)
  {
    hasValidId = saveaccount();
  }
  wifiManager.process();
  if(WiFi.status() == WL_CONNECTED && enableOTAStart)
  {
    if(otastart)
    {
      handleOTA();
    }
    else
    {
      enableOTA();
      otastart = true;
    }
  }
  if(hasValidId && (WiFi.status() != WL_CONNECTED || !pubSubClient.connected()))
  {
    for(unsigned int i(0); i<qtdSensors; i++)
    {/*
      if(!existQueue(i))
      {
      // Serial.println("Starting Queue Sensor" + String(i));
      startQueue(i);
      delay(2000);
      }
      else
      {
        if(sensors[i]->getQueuen()<int(15000/qtdSensors))
        {
          smartQueue(false);
        }
      }
      */
    }
  }

  if (!pubSubClient.connected())
  {
    reconnect();
  }
  else if (logged)
  {
    for (unsigned int i = 0; i < qtdSensors; i++)
    {/*
      if(existQueue(i))
      {
        smartQueue(true);
      }
      */
      unsigned long now = millis();
      boolean timeout = abs(float(now - (sensors[i]->getLastSendTime()))) >= (sensors[i]->getTimeout() * 1000);
      String newValue = sensors[i]->callCallback();
      boolean deadband = false;
      if (sensors[i]->getType().compareTo("number"))
      {
        deadband = abs(newValue.toFloat() - sensors[i]->getValue().toFloat()) >= sensors[i]->getDeadband();
      }
      if (timeout || deadband)
      {
        String sensorId = sensors[i]->getId();
        sendData(sensorId, newValue);
        sensors[i]->setLastSendTime(now);
      }
    }
  }
  
  return pubSubClient.loop();
}

void Saiot::connectWifi()
{

  wifiManager.addParameter(&custom_email);
  wifiManager.addParameter(&custom_senha);

  WiFi.mode(WIFI_STA);

  wifiManager.setConfigPortalBlocking(false);
  wifiManager.setConfigPortalTimeout(300);

  if(wifiManager.autoConnect("SaiotDevice"))
  {
    // Serial.println("Connectado ao WIFI");
  }
  else
  {
    // Serial.println("Portal ativado");
  }

  wifiManager.startConfigPortal("SaiotDevice");
}

bool Saiot::saveaccount()
{
    File any;

    strcpy(emailr, custom_email.getValue());
    email = emailr;

    if(email.length()<5)
    {
      return false;
    }
    
    any = LittleFS.open("e","w");

    if (!any) 
    {
        //Serial.println("createFile failed for " + String (email));
        return false;
    }
    //Serial.println(email);
    any.print(email);

    any.close();
    File any2;
    strcpy(senhar, custom_senha.getValue());
    password = senhar;
    any2  = LittleFS.open("s","w");
    if (!any2) 
    {
        //Serial.println("createFile failed for " + String (password));
        return false;
    }

    //Serial.println(password);
    any2.print(password);

    any2.close();

    firsttime = true;

    return true;
}

bool Saiot::checkaccount()
{
  String pathToSaiotId = "saiotId";
    // Serial.println("Lendo arquivo: " + pathToSaiotId);

    File file1 = LittleFS.open(pathToSaiotId, "r");
    File file2 = LittleFS.open("e", "r");
    File file3 = LittleFS.open("s", "r");
    if (file1 || file2 || file3)
    {
      hasValidId = true;
      hasValidId2 = true;

      while (file1.available())
      {
        id += (char)file1.read();
      }
      file1.close();


      while (file2.available())
      {
        email += (char)file2.read();
      }
      file2.close();


      while (file3.available())
      {
        password += (char)file3.read();
      }
      file3.close();

      return true;

    }
    else
    {
      return false;

    }
}

void Saiot::connectMqtt()
{
  pubSubClient.disconnect();

  do
  {
    //Serial.println("Tentando conectar com o broker...");
    pubSubClient.connect(id.c_str(), email.c_str(), password.c_str());
  } while (false);

  subscribe((id + "/message").c_str());
  subscribe((id + "/config").c_str());
  subscribe((id + "/act").c_str());

  //Serial.println("Conectado no broker!");
}

void Saiot::reconnect()
{
  if (!pubSubClient.connected())
  {
    //Serial.println("Conexão perdida com o broker, tentando se reconectar...");

    if (pubSubClient.connect(id.c_str(), email.c_str(), password.c_str()))
    {
      //Serial.println("Conectado no broker!");

      subscribe((id + "/message").c_str());
      subscribe((id + "/config").c_str());
      subscribe((id + "/act").c_str());

    }
    else
    {
      if(firsttime)
      {
        LittleFS.end();
        LittleFS.format();
        ESP.restart();
      }

      //Serial.print("Falha ao se conectar ao broker, rc=");
      //Serial.print(pubSubClient.state());
      //Serial.println(" tentando novamente em 5 segundos");
    }
  }
}

String Saiot::json()
{
  String json = "";

  json += "{\"id\":\"";
  json += id;

  json += "\",\"email\":\"";
  json += email;

  json += "\",\"name\":\"";
  json += deviceName;

  json += "\",\"class\":\"";
  json += deviceClass;

  json += "\",\"description\":\"";
  json += deviceDescription;

  json += "\",\"sensors\":{";

  for (unsigned int i = 0; i < qtdSensors; i++)
  {
    json += sensors[i]->json();
    if (i != qtdSensors - 1)
    {
      json += ",";
    }
  }

  json += "}";

  json += ",\"actuators\":{";

  for (unsigned int i = 0; i < qtdActuators; i++)
  {
    json += actuators[i]->json();
    if (i != qtdActuators - 1)
    {
      json += ",";
    }
  }

  json += "}";

  json += "}";

  return json;
}

void Saiot::subscribe(const char *topic)
{
  boolean subscribe = 0;
  do
  {
    //Serial.print("Tentando se inscrever no topico ");
    //Serial.println(topic);

    subscribe = pubSubClient.subscribe(topic);
    if (!subscribe)
    {
      //Serial.println((String) "Falha ao se inscrever no tópico " + topic + ", tentando de novo em 5 segundos...");
      delay(5000);
    }

  } while (pubSubClient.connected() && !subscribe);

  if (pubSubClient.connected() && subscribe)
  {
    //Serial.println((String) "Inscrito com sucesso no topico " + topic);
  }
}

void Saiot::sendMessage(const char *topic, const char *payload)
{
  boolean publish = false;
  do
  {
    //Serial.print("Envio no topico ");
    //Serial.print(topic);
    //Serial.print(" com payload ");
    //Serial.println(payload);

    publish = pubSubClient.publish(topic, payload);
    if (!publish)
    {
      // Serial.println("Falha ao enviar mensagem, tentando de novo em 5 segundos...");
      delay(5000);
    }
  } while (pubSubClient.connected() && !publish);
}

void Saiot::mqttCallback(char *topic, byte *payload, unsigned int length)
{
  // Parseando payload
  String parsedPayload = "";
  for (unsigned int i = 0; i < length; i++)
  {
    parsedPayload += (char)payload[i];
  }

  //Serial.println((String) "Mensagem recebida no tópico '" + topic + "' com payload '" + parsedPayload + "'");

  //Checando se dispositivo ja possui um id valido
  if (!hasValidId2)
  {
    //Serial.println("Recebido um novo id, reconectando...");
    id = parsedPayload;
    //Serial.println(id);
    hasValidId2 = true;
    File file = LittleFS.open("saiotId", "w");
    if (file)
    {
      if (file.print(id))
      {
        //Serial.println("Id gravado na memória com sucesso!");
      }
      else
      {
        //Serial.println("Falha ao escrever id no sistema de arquivos");
      }
    }
    else
    {
      // Serial.println("Falha ao abrir para escrita o arquivo contendo id");
    }

    file.close();

    connectMqtt();
  }
  else
  {
    // Tratativa dos tópicos
    if (String(topic).compareTo(id + "/message") == 0)
    {
      if (parsedPayload.compareTo("Cadastre-se") == 0)
      {
        logged = false;
        // Serial.println("Enviando json de configuração");
        sendMessage("register-device", json().c_str());
      }
      else if (parsedPayload.compareTo("Aprovado") == 0)
      {
        logged = false;
        if(firsttime)
        {
          firsttime = false;
        }
        // Serial.println("Dispositivo aprovado!");
        connectMqtt();
      }
      else if (parsedPayload.compareTo("Logado") == 0)
      {
        logged = true;
        // Serial.println("Dispositivo logado!");
      }
      else if (parsedPayload.compareTo("Recusado") == 0)
      {
        logged = false;
        // Serial.println("Dispositivo recusado!");
      }
      else if (parsedPayload.compareTo("Deletado") == 0)
      {
        logged = false;
        LittleFS.end();
        LittleFS.format();
        ESP.restart();
        // Serial.println("Dispositivo deletado!");
      }
      else if (parsedPayload.compareTo("Aguardando aprovação") == 0)
      {
        logged = false;
        // Serial.println("Dispositivo aguardando aprovação...");
      }
    }
    else if (String(topic).compareTo(id + "/config") == 0)
    {
      DynamicJsonDocument parsedJsonPayload(512);
      DeserializationError error = deserializeJson(parsedJsonPayload, parsedPayload);

      if (error)
      {
        // Serial.println(F("deserializeJson() failed: "));
        // Serial.println(error.f_str());
        return;
      }

    const char* deviceName_n = parsedJsonPayload["name"];
    deviceName = deviceName_n;
    //Serial.println(deviceName);
    const char* deviceClass_n = parsedJsonPayload["class"];
    deviceClass = deviceClass_n;
    //Serial.println(deviceClass);
    const char* deviceDescription_n = parsedJsonPayload["description"];
    deviceDescription = deviceDescription_n;
    //Serial.println(deviceDescription);

    for (unsigned int i = 0; i < qtdSensors; i++)
    {
    long timeout_n = parsedJsonPayload["sensors"][sensors[i]->getId()]["timeout"];
    sensors[i]->setTimeout(timeout_n);
    int deadband_n = parsedJsonPayload["sensors"][sensors[i]->getId()]["deadband"];
    sensors[i]->setDeadband(deadband_n);

    //Serial.println(timeout_n);
    //Serial.println(deadband_n);
    }

    }
    else if (String(topic).compareTo(id + "/act") == 0)
    { 
      DynamicJsonDocument parsedJsonPayload(200);
      DeserializationError error = deserializeJson(parsedJsonPayload, parsedPayload);


      if (error)
      {
        // Serial.println(F("deserializeJson() failed: "));
        // Serial.println(error.f_str());
        return;
      }

      const char *actuatorId = parsedJsonPayload["id"];
      // Serial.println((String) "Recebido ação para atuador com id " + actuatorId);

      //char **actuatorParameters = parsedJsonPayload["parameters"];
      JsonArray array = parsedJsonPayload["parameters"].as<JsonArray>();
      String actuatorParameters[array.size()];

      for (unsigned int i = 0; i < array.size(); i++)
      {
        actuatorParameters[i] = array[i].as<String>();
      }

      for (unsigned int i = 0; i < qtdActuators; i++)
      {
        if (actuators[i]->getId().compareTo(actuatorId) == 0)
        {
          String result = actuators[i]->callCallback(actuatorParameters);

          String actReturnTopic = (String) "act-return/" + id;
          String newPayload = (String) "{\"id\":\"" + actuatorId + "\",\"value\":" + result + "}";

          sendMessage(actReturnTopic.c_str(), newPayload.c_str());
        }
      }
    }
  }
}

String Saiot::formatData(String id, String value)
{
  String json = "{\"id\":\"";
  json += id;

  json += "\", \"value\":\"";
  json += value;
  json += "\"}";

  return json;
}

String Saiot::formatData(String id, String value, String time)
{
  String json = "{\"id\":\"";
  json += id;

  json += "\", \"value\":\"";
  json += value;

  json += "\", \"time\":\"";
  json += time;

  json += "\"}";

  return json;
}

void Saiot::sendData(String sensorId, String data)
{
  String topic = "measurements/" + id;
  String formatedData = formatData(sensorId, data);
  sendMessage(topic.c_str(), formatedData.c_str());
}

void Saiot::sendData(String sensorId, String data, String time)
{
  String topic = "measurements/" + id;
  String formatedData = formatData(sensorId, data, time);
  sendMessage(topic.c_str(), formatedData.c_str());
}

void Saiot::actState(const char *topic, const char *payload, int actnumb, String newState)
{
  String actReturnTopic = (String) "act-return/" + id;
  String newPayload = (String) "{\"id\":\"" + actuators[actnumb]->getId() + "\",\"value\":" + newState + "}";

  sendMessage(actReturnTopic.c_str(), newPayload.c_str());
}
/****************** WIP ***********************
String Saiot::formattedTime(time_t temp)
{
  struct tm * tmstruct = localtime(&temp);
  String formatado;

  int ano = tmstruct->tm_year + 1900;
  int mes = tmstruct->tm_mon + 1;
  int dia = tmstruct->tm_mday;
  int hora = tmstruct->tm_hour;
  int min = tmstruct->tm_min;
  int sec = tmstruct->tm_sec;
  if(sec<10)
  {
    formatado = String(dia) + "/" + String(mes) + "/" + String(ano) + "  " + String(hora) + ":" + String(min) + ":0" + String(sec);
  }
  else
  {
    formatado = String(dia) + "/" + String(mes) + "/" + String(ano) + "  " + String(hora) + ":" + String(min) + ":" + String(sec);
  }
  
  return formatado;
}

bool Saiot::existQueue(int i)
{
  File file = LittleFS.open("/" + String(i) + "/" + "configQ.txt", "r");
  if(!file)
  {
    return false;
  }
  file.close();
  return true;
}

void Saiot::startQueue(int i)
{
  sensors[i]->createFile("/" + String(i) + "/" + "configQ.txt", "0,0");
}

void Saiot::finishQueue(int i)
{
  sensors[i]->deleteFile("/" + String(i) + "/" + "configQ.txt");
}

bool Saiot::recQueue(int i)
{
  String numb1;
  String numb2;
  time_t ctime;
  bool fnumb = true;
  String where = sensors[i]->readFile("/" + String(i) + "/" + "configQ.txt", ctime);
  for(unsigned int j(0); j<strlen(where.c_str());j++)
  {
    if((char)where[j] == ',')
    {
      fnumb = false;
    }
    if(fnumb)
    {
      numb1 += (char)where[j];       
    }
    else
    {
      if(where[j] != ',')
      {
        numb2 += (char)where[j]; 
      }
    }
    
  }

  sensors[i]->setSentqueuen(numb1.toInt());
  sensors[i]->setQueuen((numb2.toInt()));
  return true;
}

void Saiot::refreshQueue(int i, bool sent)
{
  String numb1;
  String numb2;
  time_t ctime;
  bool fnumb = true;
  String where = sensors[i]->readFile("/" + String(i) + "/" + "configQ.txt", ctime);
  for(unsigned int j(0); j<strlen(where.c_str());j++)
  {
    if((char)where[j] == ',')
    {
      fnumb = false;
    }
    if(fnumb)
    {
      numb1 += (char)where[j];       
    }
    else
    {
      if(where[j] != ',')
      {
        numb2 += (char)where[j]; 
      }
    }
    
  }

  
  if(sent)
  {
    finishQueue(i);
    if(((numb1.toInt()) + 1) == numb2.toInt())
    {
      sensors[i]->setQueuen(0);
      sensors[i]->setSentqueuen(0);
      return;
    }
    sensors[i]->createFile("/" + String(i) + "/" + "configQ.txt", String((numb1.toInt()) + 1) + "," + numb2);
    sensors[i]->setSentqueuen(numb1.toInt());
  }
  else
  {
    finishQueue(i);
    sensors[i]->createFile("/" + String(i) + "/" + "configQ.txt", numb1 + "," + String((numb2.toInt()) + 1));
    sensors[i]->setQueuen((numb2.toInt()) + 1);
  }
}

void Saiot::smartQueue(bool senddata)
{
  if(!senddata)
  {
    for (unsigned int i = 0; i < qtdSensors; i++)
    {
      unsigned long now = millis();
      boolean timeout = abs(float(now - (sensors[i]->getLastSendTime()))) >= (sensors[i]->getTimeout() * 1000);
      String newValue = sensors[i]->callCallback();
      boolean deadband = false;
      if (sensors[i]->getType().compareTo("number"))
      {
        deadband = abs(newValue.toFloat() - sensors[i]->getValue().toFloat()) >= sensors[i]->getDeadband();
      }
      if (timeout || deadband)
      {
        sensors[i]->createFile("/" + String(i) + "/" + String(sensors[i]->getQueuen()), newValue);
        refreshQueue(i, false);
        sensors[i]->setLastSendTime(now);
      }
    }
  }
  else
  {
    for (unsigned int i = 0; i < qtdSensors; i++)
    {
      String toSend;
      time_t creattime;
      time(&creattime);
      // Serial.println(formattedTime(creattime));
      toSend = sensors[i]->readFile("/" + String(i) + "/" + String(sensors[i]->getSentqueuen()), creattime);
      // Serial.println(toSend);
      sendData(sensors[i]->getId(),toSend,formattedTime(creattime));
      sensors[i]->deleteFile("/" + String(i) + "/" + String(sensors[i]->getSentqueuen()));
      refreshQueue(i, true);
    }
  }
}
*/
void Saiot::enableOTA()
{
  ArduinoOTA.onStart([]() {
    Serial.println("Iniciando Server OTA");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("Fechando Server OTA!");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progresso: %u%%r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Erro [%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Autenticacao Falhou");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Falha no Inicio");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Falha na Conexao");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Falha na Recepcao");
    else if (error == OTA_END_ERROR) Serial.println("Falha no Fim");
  });
  ArduinoOTA.begin();
  // Serial.println("OTA Funcionando");
  // Serial.print("Endereco IP: ");
  Serial.println(WiFi.localIP());
}

void Saiot::handleOTA()
{
  ArduinoOTA.handle();
}

void Saiot::disableOTA()
{
  enableOTAStart = false;
}
