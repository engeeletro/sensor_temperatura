#include "Actuator.h"

Actuator::Actuator(String id, String name, String type, std::vector<String> value, CALLBACK_ACTUATOR_TYPE callback)
{
    this->id = id;
    this->name = name;
    this->type = type;
    this->value = value;
    this->callback = callback;
}
Actuator::~Actuator() {}

String Actuator::json()
{
    String json = "";

    json += "\"";
    json += id;
    json += "\"";
    json += ":{";

    json += "\"name\":\"";
    json += name;

    json += "\",\"type\":\"";
    json += type;

    json += "\",\"value\": [";
    
    for(unsigned int i =0; i<value.size(); i++)
    {
        json += value[i];
        if(i != value.size()-1){
        json += ",";
        }   
    }

    json += "]}";

    return json;
}

String Actuator::getId()
{
    return id;
}

String Actuator::callCallback(String value[])
{
    return callback(value);
}
