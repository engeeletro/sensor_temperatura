# Biblioteca de dispositivos Saiot

## Criando o ambiente 

### Instalando o platformIO

Primeiramente baixe uma IDE compativel, nós recomendamos o uso do editor de texto [Visual Studio Code](https://code.visualstudio.com/). 
Caso você opte por um outro editor, siga os passos no site do [platformIO](https://platformio.org/)

No Visual Studio Code basta que você vá na aba de extensões e instale a extansão do platformIO

Depois de ter uma IDE com plaftformIO basta baixar o main desse repositorio e você terá seu ambiente preparado.

OBS: É possível usar a IDE do arduino porem outros passos devem ser usados que não são descritos nesse tutorial.

## Utilizando a biblioteca

Baixe o main desse repositorio e abra na sua IDE.

Dentro da pasta basta que você vá na aba src e edite o main.cpp. Encontraremos algo como o código abaixo

```c++
#include "Saiot.h"

String deviceName = "nomedisp";
String deviceClass = "classedispositivo";
String deviceDescription = "descrição";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do atuador
//String [namedevice]Callback(String value[]);

void setup()
{
  Serial.begin(9600);
//  Actuator/Sensor *[namedevice] = new Actuator/Sensor("id","name", "type", {}, [namedevice]Callback);
//  saiot.addActuator/Sensor(*led);
//  saiot.disableOTA(); caso não seja necessário
  saiot.start();
}

void loop()
{
  saiot.loop();
}

```
Para que tenhamos um dispotivos unico devemos dá um nome, uma classe e uma breve descrição!!!

Temos também que criar um callBack um sensor ou um actuador.

Abaixo iremos descrever esses passos de forma mais detalhada.


### Nomeando Seu dispositivo

Precisamos definir pinos de saida ou entrada no esp8266.

Mudando as variaveis deviceName, deviceClass, deviceDescription!!!

```c++
#include "Saiot.h"

#define LED_PIN 2

String deviceName = "LED";
String deviceClass = "lampada";
String deviceDescription = "LED do ESP8266";

Saiot saiot(deviceName, deviceClass, deviceDescription);

```

### Criando a função callback

A função callback é responsavel por definir o que um sensor ou atuador envia ao SAIOT. 

Para isso é preciso criar uma função que retorne uma String!!!

Um exemplo de uma função que faz o led interno do esp piscar esta descrita abaixo.


```c++

String ledSwitchCallback(String value[]);

void setup()
{
    (...)
}

void loop()
{
    (...)
}

String ledSwitchCallback(String value[])
{
  String ledState = value[0];

  Serial.println("Recebido valor no atuador ledSwitch: " + ledState);

  if (ledState.compareTo("true") == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return "true";
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    return "false";
  }
}
```

### Criando seu sensor/atuador

Para criar nosso sensor/actuador devemos dar-lhe:

Um id que terá uso interno;

Um nome que será exibido no SAIOT;

Um dos tipos predefinidos no SAIOT;

    Para actuators:
    number: Que criar um actuador de numeros!!!
    switch: Que gera um botão transitorio entre dois estados!!!
    color: Que gera uma escala rgbx!!!
    slider: Que gera um deslizador de valores inteiros!!!
    custom: Que gera um actuator customizado!!!

** **

    Para Sensores:
    number: Cria um sensor que recebe valores numericos!!!
    on/off: Cria um sensor que recebe booleanos!!!
    gps: Cria um sensor que recebe Lat e Long e o transforma em um mapa!!!

E o Callback que criamos no passo anterios.

Um exemplo pode ser visto no código abaixo:

```c++

void setup()
{
  Serial.begin(9600);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  Actuator *led = new Actuator("LedSwitch", "LED", "switch", {"false"}, ledSwitchCallback);
  saiot.addActuator(*led);
  saiot.start();
}

void loop()
{
  saiot.loop();
}


```

### Código Completo

Com tudo que fizemos até agora temos um esp8266 capaz de conectar a plataforma SAIOT e ser controlado pelo sistema.

O código completo pode ser visto abaixo:
```c++
#include "Saiot.h"

#define LED_PIN 2

String deviceName = "LED";
String deviceClass = "lampada";
String deviceDescription = "LED do ESP8266";

Saiot saiot(deviceName, deviceClass, deviceDescription);

// Callback do atuador
String ledSwitchCallback(String value[]);

void setup()
{
  Serial.begin(9600);

  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  Actuator *led = new Actuator("LedSwitch", "LED", "switch", {"false"}, ledSwitchCallback);
  saiot.addActuator(*led);
  saiot.start();
}

void loop()
{
  saiot.loop();
}

String ledSwitchCallback(String value[])
{
  String ledState = value[0];

  Serial.println("Recebido valor no atuador ledSwitch: " + ledState);

  if (ledState.compareTo("true") == 0)
  {
    digitalWrite(LED_PIN, LOW);
    return "true";
  }
  else
  {
    digitalWrite(LED_PIN, HIGH);
    return "false";
  }
}


```

# Conectando com o Saiot

Quando terminamos nosso código, utilizamos o platformIO para fazer a compilação e transferencia do código para o ESP8266.

Utilizando a opção upload em monitor para abrirmos o serial.

<img src="https://www.imagemhost.com.br/images/2022/06/01/Screenshot-from-2022-06-01-11-40-36.png" width="300">



Uma vez o código transferido podemos ligar o esp8266 e conectar na rede que ele irá criar por 5 minutos chamada SAIOTDEVICE.


<img src="https://i.ibb.co/zP05cr4/20220517-100237.jpg" width="300">


Nessa rede colocaremos nome e senha da rede de WIFI que utilizaremos. E logo abaixo nossas credenciais do SAIOT.

E em poucos segundos teremos um dispositivo funcional conectado ao SAIOT.
